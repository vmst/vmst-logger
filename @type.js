// @ts-check
exports.stub = stub
function stub () {}

const lib_vmst_driver = require('vmst-driver')

/**
 * @typedef constructor_options
 * @property {string} [app_name]
 * @property {string} [scope_name]
 * @property {sql_connection_options} [sql_connection_option]
 * @property {boolean} [print_to_console]
 */

/**
 * @typedef sql_connection_options
 * @property {string} instance ms sql instance, examples - 'localhost', 'myserver/myinstance'
 * @property {string} [login] login for ms sql authentication, if need domain authentication, set undefined
 * @property {string} [password] password for ms sql authentication, if need domain authentication, set undefined
 * @property {string} [database] database name for storage log, default - 'tempdb'
 * @property {string} [schema] tables schema name for storage log, default - 'vmst'
 * @property {string} [table] prefix table names for storage log, default - 'log', tables will be created - 'log', 'log_pipe', 'log_trace'
 * @property {string} [app_name] app name, which will be visible in profiler, default - 'vmst-logger'
 */

/**
 * @typedef private
 * @property {string} app_name
 * @property {string} scope_name
 * @property {string[]} pipe_list
 * @property {boolean} allow_empty_pipe
 * @property {log[]} log_list
 * @property {number} file_time_save_sec
 * @property {number} file_time_life_day
 * @property {number} sql_time_save_sec
 * @property {number} sql_time_life_sec
 * @property {file} file_mode
 * @property {sql} sql_mode
 * @property {boolean} [print_to_console]
 */

/**
 * @typedef file
 * @property {'on'|'on_if_no_mssql'|'off'} mode
 * @property {string} path
 * @property {string} path_trace
 * @property {string} suffix
 */

/**
 * @typedef sql
 * @property {lib_vmst_driver} sql
 * @property {sql_connection_options} connection_options
 * @property {number} local_timezone
 * @property {boolean} last_write_log_has_error
 */

/**
 * @typedef log
 * @property {string} data
 * @property {string} verbal_message
 * @property {string[]} pipe_list
 * @property {log_trace[]} trace_list
 * @property {boolean} is_error
 * @property {Date} fdm
 * @property {boolean} write_to_file
 * @property {boolean} write_to_mssql
 * @property {boolean} last_write_log_has_error
 * @property {string} file_name_trace
 */

/**
 * @typedef log_trace
 * @property {string} kind
 * @property {string} data
 */

/**
 * @typedef file_list
 * @property {string} full_file_name
 * @property {string} text
 */

/**
 * @typedef sql_information_schema_columns
 * @property {string} table_name,
 * @property {string} column_name,
 * @property {boolean} is_nullable,
 * @property {string} data_type,
 * @property {number} character_maximum_length
 */