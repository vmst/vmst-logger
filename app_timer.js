//@ts-check
let lib_conv = require('viva-convert')
let app = require('./app.js')
let app_file = require('./app_file.js')
let app_sql = require('./app_sql.js')

/** @type {NodeJS.Timeout} */
let timer_save_to_file = undefined
/** @type {NodeJS.Timeout} */
let timer_delete_old_file = undefined
/** @type {NodeJS.Timeout} */
let timer_save_to_sql = undefined
/** @type {NodeJS.Timeout} */
let timer_delete_old_sql = undefined
/** @type {NodeJS.Timeout} */
let timer_clear_list = undefined

exports.clear_list = clear_list
exports.save_to_file_stop = save_to_file_stop
exports.save_to_file_start = save_to_file_start
exports.delete_old_file_stop = delete_old_file_stop
exports.delete_old_file_start = delete_old_file_start
exports.save_to_sql_stop = save_to_sql_stop
exports.save_to_sql_start = save_to_sql_start
exports.delete_old_sql_stop = delete_old_sql_stop
exports.delete_old_sql_start = delete_old_sql_start

/**
 * @param {app} app
 */
function clear_list (app) {
    if (!lib_conv.isAbsent(timer_clear_list)) {
        clearTimeout(timer_clear_list)
        timer_clear_list = undefined
    }
    timer_clear_list = setTimeout(function tick() {
        let exists_sql = !lib_conv.isAbsent(app._private.sql_mode.sql)
        switch (app._private.file_mode.mode) {
            case 'off':
                if (exists_sql) {
                    app._private.log_list = app._private.log_list.filter(f => f.write_to_mssql === false)
                } else {
                    app._private.log_list = []
                }
                break
            case 'on_if_no_mssql':
                if (exists_sql) {
                    app._private.log_list = app._private.log_list.filter(f => f.write_to_mssql === false)
                } else {
                    app._private.log_list = app._private.log_list.filter(f => f.write_to_file === false)
                }
                break
            case 'on':
                if (exists_sql) {
                    app._private.log_list = app._private.log_list.filter(f => f.write_to_mssql === false || f.write_to_file === false)
                } else {
                    app._private.log_list = app._private.log_list.filter(f => f.write_to_file === false)
                }
                break
            default:
                app._private.log_list = app._private.log_list.filter(f => f.write_to_file === false || f.write_to_mssql === false)
        }

        timer_clear_list = setTimeout(tick, 1000 * 20)
    }, 1000 * 20)
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function save_to_file_stop(app, callback) {
    app_file.save(app, () => {
        if (!lib_conv.isAbsent(timer_save_to_file)) {
            clearTimeout(timer_save_to_file)
            timer_save_to_file = undefined
        }
        if (lib_conv.isFunction(callback)) {
            callback()
        }
    })
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function save_to_file_start(app, callback) {
    save_to_file_stop(app, () => {
        if (app._private.file_mode.mode === 'off') {
            if (lib_conv.isFunction(callback)) {
                callback()
            }
            return
        }
        let timeout = app._private.file_time_save_sec
        if (timeout < 1) {
            timeout = 1
        }
        let first_timeout = timeout
        if (first_timeout > 5) {
            first_timeout = 5
        }
        timer_save_to_file = setTimeout(function tick() {
            app_file.save(app, () => {
                timer_save_to_file = setTimeout(tick, timeout * 1000)
            })
        }, first_timeout * 1000)
        if (lib_conv.isFunction(callback)) {
            callback()
        }
    })
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function delete_old_file_stop(app, callback) {
    if (!lib_conv.isAbsent(timer_delete_old_file)) {
        clearTimeout(timer_delete_old_file)
        timer_delete_old_file = undefined
    }
    if (lib_conv.isFunction(callback)) {
        callback()
    }
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function delete_old_file_start(app, callback) {
    delete_old_file_stop(app, () => {
        let timeout = 1000 * 60 * 60 * 3 //3 hour
        let first_timeout = 1000 * 60 // 1 min
        timer_delete_old_file = setTimeout(function tick() {
            app_file.remove(app, () => {
                timer_delete_old_file = setTimeout(tick, timeout)
            })
        }, first_timeout)
        if (lib_conv.isFunction(callback)) {
            callback()
        }
    })
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function save_to_sql_stop(app, callback) {
    app_sql.save(app, () => {
        app._private.sql_mode.sql = undefined
        app._private.sql_mode.last_write_log_has_error = false
        if (!lib_conv.isAbsent(timer_save_to_sql)) {
            clearTimeout(timer_save_to_sql)
            timer_save_to_sql = undefined
        }
        if (lib_conv.isFunction(callback)) {
            callback()
        }
    })
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function save_to_sql_start(app, callback) {
    save_to_sql_stop(app, () => {
        app_sql.connect(app, () => {
            if (lib_conv.isAbsent(app._private.sql_mode.sql)) {
                if (lib_conv.isFunction(callback)) {
                    callback()
                }
                return
            }
            let timeout = app._private.sql_time_save_sec
            if (timeout <= 0) {
                timeout = 5
            }
            let first_timeout = timeout
            if (first_timeout > 5) {
                first_timeout = 5
            }
            timer_save_to_sql = setTimeout(function tick() {
                app_sql.save(app, () => {
                    timer_save_to_sql = setTimeout(tick, timeout * 1000)
                })
            }, first_timeout * 1000)
            if (lib_conv.isFunction(callback)) {
                callback()
            }
        })
    })
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function delete_old_sql_stop(app, callback) {
    if (!lib_conv.isAbsent(timer_delete_old_sql)) {
        clearTimeout(timer_delete_old_sql)
        timer_delete_old_sql = undefined
    }
    if (lib_conv.isFunction(callback)) {
        callback()
    }
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function delete_old_sql_start(app, callback) {
    delete_old_sql_stop(app, () => {
        let timeout = 1000 * 60 * 60 * 3 //3 hour
        if (app._private.sql_time_life_sec < (60 * 10)) { // < 10 min
            timeout = 1000 * 60 // 1 min
        } else if (app._private.sql_time_life_sec < (60 * 60)) { // < 1 hour
            timeout = 1000 * 60 * 10 // 10 min
        } else if (app._private.sql_time_life_sec < (60 * 60 * 3)) { // < 3 hour
            timeout = 1000 * 60 * 30 // 30 min
        } else if (app._private.sql_time_life_sec < (60 * 60 * 24)) { // < 1 day
            timeout = 1000 * 60 * 60 // 1 hour
        }
        let first_timeout = 1000 // 1 min

        timer_delete_old_sql = setTimeout(function tick() {
            app_sql.remove(app, () => {
                timer_delete_old_sql = setTimeout(tick, timeout)
            })
        }, first_timeout)
        if (lib_conv.isFunction(callback)) {
            callback()
        }
    })
}
