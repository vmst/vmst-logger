//@ts-check

const lib_os = require('os')
const lib_conv = require('viva-convert')
const lib_vmst_driver = require('vmst-driver')
const lib_vmst_helper = require('vmst-helper')
const helper = require('vmst-helper')
const app = require('./app.js')

let save_process = false

exports.connect = connect
exports.save = save
exports.remove = remove

/**
 * @param {app} app
 * @param {function} callback
 */
function connect(app, callback) {
    app._private.sql_mode.sql = undefined

    if (lib_conv.isEmpty(app._private.sql_mode.connection_options.instance)) {
        callback()
        return
    }

    let sql = new lib_vmst_driver({
        instance: app._private.sql_mode.connection_options.instance,
        login: app._private.sql_mode.connection_options.login,
        password: app._private.sql_mode.connection_options.password,
        additional: {
            database: app._private.sql_mode.connection_options.database,
            app_name: app._private.sql_mode.connection_options.app_name
        }
    })
    sql.ping(error => {
        if (!lib_conv.isAbsent(error)) {
            callback()
            return
        }

        let schema = app._private.sql_mode.connection_options.schema
        let query_schema = [
            lib_vmst_helper.depot_sch_schema(schema),
            lib_vmst_helper.depot_sch_table(schema, table_name(app, 'log'), 'log storage: main table', [
                {name: 'rid', type: 'uniqueidentifier', nullable: false, pk_position: 1, description: 'primary key'},
                {name: 'app', type: 'varchar', nullable: false, len_chars: 20, description: 'author: app'},
                {name: 'scope', type: 'varchar', nullable: false, len_chars: 10, description: 'author: scope'},
                {name: 'data', type: 'nvarchar', nullable: false, len_chars: 'max', description: 'message'},
                {name: 'is_error', type: 'bit', nullable: false, description: 'error or debug'},
                {name: 'fdm', type: 'datetime', nullable: false, description: 'date/time create'},
                {name: 'lat', type: 'timestamp', nullable: false},
            ], 'error'),
            lib_vmst_helper.depot_sch_table(schema, table_name(app, 'pipe'), 'log storage: pipes', [
                {name: 'parent_rid', type: 'uniqueidentifier', nullable: false, pk_position: 1},
                {name: 'pipe', type: 'varchar', nullable: false, len_chars: 100, pk_position: 2},
            ], 'error'),
            lib_vmst_helper.depot_sch_table(schema, table_name(app, 'trace'), 'log storage: traces', [
                {name: 'parent_rid', type: 'uniqueidentifier', nullable: false, pk_position: 1},
                {name: 'kind', type: 'varchar', nullable: false, len_chars: 100, pk_position: 2},
                {name: 'data', type: 'nvarchar', nullable: false, len_chars: 'max', description: 'trace'},
                {name: 'lat', type: 'timestamp', nullable: false},
            ], 'error'),
            lib_vmst_helper.depot_sch_foreign(schema, table_name(app, 'pipe'), schema, table_name(app, 'log'), [{child_column: 'parent_rid', parent_column: 'rid'}], 'cascade', 'cascade'),
            lib_vmst_helper.depot_sch_foreign(schema, table_name(app, 'trace'), schema, table_name(app, 'log'), [{child_column: 'parent_rid', parent_column: 'rid'}], 'cascade', 'cascade'),
        ]

        sql.exec(query_schema, undefined, callback_exec => {
            if (callback_exec.type === 'end') {
                if (!lib_conv.isEmpty(callback_exec.end.error)) {
                    app.error('on update schema in MS SQL', undefined, [{kind: 'message', data: lib_conv.toErrorMessage(callback_exec.end.error)}, {kind: 'query', data: callback_exec.end.get_beauty_query('actual')}]  )
                    callback()
                    return
                }
                app._private.sql_mode.sql = sql
                callback()
                return
            }
        })
    })
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function save(app, callback) {
    if (lib_conv.isAbsent(app._private.sql_mode.sql)) {
        if (lib_conv.isFunction(callback)) {
            callback()
        }
        return
    }
    if (save_process !== false) {
        let t = setTimeout(() => {
            clearTimeout(t)
            t = undefined
            save(app, callback)
        }, 500)
        return
    }
    save_process = true

    let log_list_for_save = app._private.log_list.filter(f => f.write_to_mssql === false)
    if (log_list_for_save.length <= 0) {
        if (lib_conv.isFunction(callback)) {
            callback()
        }
        save_process = false
        return
    }

    app._private.sql_mode.sql.newid(log_list_for_save.length, (error, guid_list) => {
        if (!lib_conv.isAbsent(error)) {
            if (app._private.sql_mode.last_write_log_has_error !== true) {
                app.error('on get guid list for save log to MS SQL', undefined, {kind: 'message', data: lib_conv.toErrorMessage(error)}, message => {
                    message.last_write_log_has_error = true
                })
            }
            app._private.sql_mode.last_write_log_has_error = true
            save_process = false
            log_list_for_save.forEach(log => {log.last_write_log_has_error = true})
            if (lib_conv.isFunction(callback)) {
                callback()
            }
            return
        }
        let query1 = []
        let query2 = []
        let query3 = []

        let time_offset = app._private.sql_mode.local_timezone + app._private.sql_mode.sql.info.timezone

        //TODO привести дату к серверному времени
        log_list_for_save.forEach((log, log_index) => {
            query1.push(
                lib_conv.format("SELECT {0}, {1}, {2}, {3}, {4}, DATEADD(MINUTE, {5}, {6})", [
                    helper.helper_js_to_sql(guid_list[log_index], 'guid'),
                    helper.helper_js_to_sql(app._private.app_name, 'varchar', true),
                    helper.helper_js_to_sql(app._private.scope_name, 'varchar', true),
                    helper.helper_js_to_sql(log.data, 'nvarchar', true),
                    helper.helper_js_to_sql(log.is_error, 'bit'),
                    helper.helper_js_to_sql(time_offset, 'int'),
                    helper.helper_js_to_sql(log.fdm, 'datetime')
                ])
            )
            if (log.pipe_list.length > 0) {
                let pipe_list = []
                log.pipe_list.filter(f => !lib_conv.isEmpty(f)).forEach(pipe => {
                    if (!pipe_list.includes(pipe.toLowerCase())) {
                        pipe_list.push(pipe)
                    }
                })
                pipe_list.forEach(pipe => {
                    query2.push(
                        lib_conv.format("SELECT {0}, {1}", [
                            helper.helper_js_to_sql(guid_list[log_index], 'guid'),
                            helper.helper_js_to_sql(pipe, 'nvarchar', true)
                        ])
                    )
                })
            }
            if (log.trace_list.length > 0) {
                let trace_list = []
                log.trace_list.filter(f => !lib_conv.isAbsent(f) && !lib_conv.isEmpty(f.kind)).forEach(trace => {
                    if (!trace_list.some(f => f.kind.toLowerCase() === trace.kind.toLowerCase())) {
                        trace_list.push(trace)
                    }
                })
                trace_list.forEach(trace => {
                    query3.push(
                        lib_conv.format("SELECT {0}, {1}, {2}", [
                            helper.helper_js_to_sql(guid_list[log_index], 'guid'),
                            helper.helper_js_to_sql(trace.kind, 'varchar', true),
                            helper.helper_js_to_sql(trace.data, 'nvarchar', true),
                        ])
                    )
                })
            }
        })

        let query = [
            "BEGIN TRAN",
            lib_conv.format("INSERT INTO [{0}].[{1}] ([rid], [app], [scope], [data], [is_error], [fdm])", [app._private.sql_mode.connection_options.schema, table_name(app, 'log')]),
            query1.join(' UNION ALL'.concat(lib_os.EOL))
        ]
        if (query2.length > 0) {
            query.push(lib_conv.format("INSERT INTO [{0}].[{1}] ([parent_rid], [pipe])", [app._private.sql_mode.connection_options.schema, table_name(app, 'pipe')]))
            query.push(query2.join(' UNION ALL'.concat(lib_os.EOL)))
        }
        if (query3.length > 0) {
            query.push(lib_conv.format("INSERT INTO [{0}].[{1}] ([parent_rid], [kind], [data])", [app._private.sql_mode.connection_options.schema, table_name(app, 'trace')]))
            query.push(query3.join(' UNION ALL'.concat(lib_os.EOL)))
        }
        query.push("COMMIT")

        app._private.sql_mode.sql.exec(query.join(lib_os.EOL), undefined, callback_exec => {
            if (callback_exec.type !== 'end') return
            if (!lib_conv.isAbsent(callback_exec.end.error)) {
                if (app._private.sql_mode.last_write_log_has_error !== true) {
                    app._private.sql_mode.last_write_log_has_error = true
                    app.error('on write log to MS SQL', undefined, [{kind: 'message', data: lib_conv.toErrorMessage(callback_exec.end.error)}, {kind: 'query', data: callback_exec.end.get_beauty_query('actual')}], message => {
                        message.last_write_log_has_error = true
                    })
                }
                log_list_for_save.forEach(log => {log.last_write_log_has_error = true})
            } else {
                if (app._private.sql_mode.last_write_log_has_error !== false) {
                    app._private.sql_mode.last_write_log_has_error = false
                    app.log('error work with MS SQL disappeared', undefined, undefined, message => {
                    })
                }
                log_list_for_save.forEach(log => {log.write_to_mssql = true})
            }
            save_process = false
            if (lib_conv.isFunction(callback)) {
                callback()
            }
        })
    })
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function remove(app, callback) {
    if (lib_conv.isAbsent(app._private.sql_mode.sql)) {
        if (lib_conv.isFunction(callback)) {
            callback()
        }
        return
    }

    let query = lib_conv.format("DELETE FROM [{0}].[{1}] WHERE app = {2} AND scope = {3} AND fdm < DATEADD(SECOND, {4}, GETDATE())", [
        app._private.sql_mode.connection_options.schema,
        table_name(app, 'log'),
        helper.helper_js_to_sql(app._private.app_name, 'varchar'),
        helper.helper_js_to_sql(app._private.scope_name, 'varchar'),
        -1 * app._private.sql_time_life_sec
    ])

    app._private.sql_mode.sql.exec(query, undefined, callback_exec => {
        if (callback_exec.type !== 'end') return
        if (!lib_conv.isAbsent(callback_exec.end.error)) {
            app.error('on delete old log from MS SQL', undefined, [{kind: 'message', data: lib_conv.toErrorMessage(callback_exec.end.error)}, {kind: 'query', data: callback_exec.end.get_beauty_query('actual')}], message => {
                message.last_write_log_has_error = true
            })
        }
        if (lib_conv.isFunction(callback)) {
            callback()
        }
    })
}

/**
 * @param {app} app
 * @param {'log'|'pipe'|'trace'} table
 * @returns {string}
 */
function table_name(app, table) {
    let t = lib_conv.border_del(lib_conv.toString(app._private.sql_mode.connection_options.table,''), '[', ']').toLowerCase().trim()
    switch (table) {
        case 'log':
            return t
        case 'pipe':
            return t.concat("_pipe")
        case 'trace':
            return t.concat("_trace")
        default:
            return ''
    }
}