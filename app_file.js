//@ts-check

const lib_fs = require('fs-extra')
const lib_path = require('path')
const lib_os = require('os')
const lib_conv = require('viva-convert')
const app = require('./app.js')
const type = require('./@type.js')

let save_process = false

exports.save = save
exports.remove = remove

/**
 * @param {app} app
 * @param {function} [callback]
 */
function save(app, callback) {
    if (lib_conv.isEmpty(app._private.file_mode.path) || lib_conv.isEmpty(app._private.file_mode.path_trace)) {
        if (lib_conv.isFunction(callback)) {
            callback()
        }
        return
    }
    if (save_process !== false) {
        let t = setTimeout(() => {
            clearTimeout(t)
            t = undefined
            save(app, callback)
        }, 500)
        return
    }
    save_process = true

    let first_index = app._private.log_list.findIndex(f => f.write_to_file === false)
    if (first_index < 0) {
        save_process = false
        if (lib_conv.isFunction(callback)) {
            callback()
        }
        return
    }
    let first_message = app._private.log_list[first_index]
    let first_file_name_prefix = get_file_name_prefix(app, first_message.fdm)

    let find_other_file_name_prefix = false

    /** @type {string[]} */
    let message_list_log = []
    /** @type {string[]} */
    let message_list_err = []
    /** @type {type.file_list[]} */
    let file_list = []

    for (let i = first_index; i < app._private.log_list.length; i++ ) {
        let message = app._private.log_list[i]
        if (message.write_to_file !== false) continue
        if (
            (app._private.file_mode.mode === 'on') ||
            (app._private.file_mode.mode === 'on_if_no_mssql' && message.write_to_mssql === false && (message.last_write_log_has_error === true || lib_conv.isAbsent(app._private.sql_mode.sql)))
            ) {
            let file_name_prefix = get_file_name_prefix(app, message.fdm)
            if (file_name_prefix != first_file_name_prefix) {
                find_other_file_name_prefix = true
                break
            }

            if (!lib_conv.isEmpty(message.file_name_trace)) {
                file_list.push({
                    full_file_name: message.file_name_trace,
                    text: message.trace_list.map(m => {
                        return [
                            '',
                            '#KIND#={'.concat(lib_conv.toString(m.kind,''),'}'),
                            '',
                            lib_conv.toString(m.data,'').trim()
                        ].join(lib_os.EOL)
                    }).join(lib_os.EOL)
                })
            }

            message_list_log.push(message.verbal_message)
            if (message.is_error === true) {
                message_list_err.push(message.verbal_message)
            }
            message.write_to_file = true
        }
    }

    if (message_list_log.length > 0) {
        message_list_log.push('')
        file_list.unshift({full_file_name: lib_path.join(first_file_name_prefix).concat('.debug.log'), text: message_list_log.join(lib_os.EOL)})
    }
    if (message_list_err.length > 0) {
        message_list_err.push('')
        file_list.unshift({full_file_name: lib_path.join(first_file_name_prefix).concat('.error.log'), text: message_list_err.join(lib_os.EOL)})
    }

    save_disk(file_list, 0, () => {
        save_process = false
        if (find_other_file_name_prefix === true) {
            save(app, callback)
        } else {
            if (lib_conv.isFunction(callback)) {
                callback()
            }
        }
    })
}
/**
 * @param {app} app
 * @param {Date} d
 * @returns {string}
 */
function get_file_name_prefix(app, d) {
    let file_name = lib_path.join(app._private.file_mode.path, lib_conv.formatDate(d, 112))
    file_name = file_name.concat(app._private.file_mode.suffix)
    return file_name
}

/**
 * @param {type.file_list[]} file_list
 * @param {number} index
 * @param {function} callback
 */
function save_disk(file_list, index, callback) {
    if (index >= file_list.length) {
        callback()
        return
    }
    lib_fs.ensureDir(lib_path.dirname(file_list[index].full_file_name), undefined, () => {
        lib_fs.appendFile(file_list[index].full_file_name, file_list[index].text, {encoding: 'utf8'}, () => {
            index++
            save_disk(file_list, index, callback)
        })
    })
}

/**
 * @param {app} app
 * @param {function} [callback]
 */
function remove(app, callback) {
    if (lib_conv.isEmpty(app._private.file_mode.path) || lib_conv.isEmpty(app._private.file_mode.path_trace)) {
        if (lib_conv.isFunction(callback)) {
            callback()
        }
        return
    }

    let d = new Date()

    let survivor_trace_path_list = [ lib_path.join(app._private.file_mode.path_trace, lib_conv.formatDate(d, 112)).toLowerCase() ]
    let survivor_file_list = [
        lib_path.join(get_file_name_prefix(app, d).concat('.error.log')).toLowerCase(),
        lib_path.join(get_file_name_prefix(app, d).concat('.debug.log')).toLowerCase(),
        lib_path.join(app._private.file_mode.path, 'trace').toLowerCase()
    ]

    for (let i = 0; i < app._private.file_time_life_day; i++) {
        d = lib_conv.dateAdd('day', -1, d)
        survivor_trace_path_list.push(lib_path.join(app._private.file_mode.path_trace, lib_conv.formatDate(d, 112)).toLowerCase())
        survivor_file_list.push(lib_path.join(get_file_name_prefix(app, d).concat('.error.log')).toLowerCase())
        survivor_file_list.push(lib_path.join(get_file_name_prefix(app, d).concat('.debug.log')).toLowerCase())
    }

    let delete_list = []

    lib_fs.readdir(app._private.file_mode.path_trace, (error, files_or_dirs) => {
        if (lib_conv.isAbsent(files_or_dirs)) {
            files_or_dirs = []
        }
        files_or_dirs.forEach(f_o_d => {
            f_o_d = lib_path.join(app._private.file_mode.path_trace, f_o_d).toLowerCase()
            if (!survivor_trace_path_list.includes(f_o_d)) {
                delete_list.push(f_o_d)
            }
        })
        lib_fs.readdir(app._private.file_mode.path, (error, files_or_dirs) => {
            if (lib_conv.isAbsent(files_or_dirs)) {
                files_or_dirs = []
            }
            files_or_dirs.forEach(f_o_d => {
                f_o_d = lib_path.join(app._private.file_mode.path, f_o_d).toLowerCase()
                if (!survivor_file_list.includes(f_o_d)) {
                    delete_list.push(f_o_d)
                }
            })
            remove_disk(delete_list, 0, () => {
                if (lib_conv.isFunction(callback)) {
                    callback()
                }
            })
        })
    })
}

/**
 * @param {string[]} delete_list
 * @param {number} index
 * @param {function} callback
 */
function remove_disk(delete_list, index, callback) {
    if (index >= delete_list.length) {
        callback()
        return
    }
    lib_fs.remove(delete_list[index], (error) => {
        index++
        remove_disk(delete_list, index, callback)
    })
}