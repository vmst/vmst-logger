//@ts-check
const lib_fs = require('fs-extra')
const lib_path = require('path')
const lib_conv = require('viva-convert')
const shared = require('vmst-shared')
const type = require('./@type.js')
const app_message = require('./app_message.js')
const app_timer = require('./app_timer.js')


/** Examples - see _demo.js */
class App {
    /**
     * @param {type.constructor_options} [options]
     */
    constructor(options) {
        let app_name = (lib_conv.isAbsent(options) ? '' : lib_conv.toString(options.app_name, ''))
        let scope_name = (lib_conv.isAbsent(options) ? '' : lib_conv.toString(options.scope_name, ''))
        let error_text_app = (lib_conv.isEmpty(app_name) ? undefined : shared.check_app_name(app_name))
        if (!lib_conv.isEmpty(error_text_app)) {
            throw new Error(error_text_app)
        }
        let error_text_scope = (lib_conv.isEmpty(scope_name) ? undefined : shared.check_scope_name(scope_name))
        if (!lib_conv.isEmpty(error_text_scope)) {
            throw new Error(error_text_scope)
        }

        let sql_app_name = lib_conv.toString(app_name, 'vmst-logger').concat((lib_conv.isEmpty(scope_name) ? '' : '.'.concat(lib_conv.toString(scope_name, ''))))

        /** @type {type.sql_connection_options} */
        let sql_connection_option = (lib_conv.isAbsent(options) || lib_conv.isAbsent(options.sql_connection_option) ? {} : options.sql_connection_option)

        /** @type {type.private} */
        this._private = {
            app_name: app_name,
            scope_name: scope_name,
            pipe_list: [],
            allow_empty_pipe: false,
            log_list: [],
            file_time_save_sec: 1,
            file_time_life_day: 1,
            sql_time_save_sec: 5,
            sql_time_life_sec: 86400,
            file_mode: {
                mode: 'on_if_no_mssql',
                path: undefined,
                path_trace: undefined,
                suffix: '',
            },
            sql_mode: {
                sql: undefined,
                connection_options: {
                    instance: lib_conv.toString(sql_connection_option.instance, ''),
                    login: lib_conv.toString(sql_connection_option.login, ''),
                    password: lib_conv.toString(sql_connection_option.password, ''),
                    database: lib_conv.border_del(lib_conv.toString(sql_connection_option.database, 'tempdb'), '[', ']'),
                    schema: lib_conv.border_del(lib_conv.toString(sql_connection_option.schema, 'vmst'), '[', ']'),
                    table: lib_conv.border_del(lib_conv.toString(sql_connection_option.table, 'log'), '[', ']'),
                    app_name: sql_app_name,
                },
                last_write_log_has_error: false,
                local_timezone: (new Date()).getTimezoneOffset()
            },
            print_to_console: (lib_conv.isAbsent(options) ? false : lib_conv.toBool(options.print_to_console, false))
        }

        if (!lib_conv.isEmpty(this._private.app_name)) {
            this._private.file_mode.suffix = this._private.file_mode.suffix.concat('.', this._private.app_name)
        }
        if (!lib_conv.isEmpty(this._private.scope_name)) {
            this._private.file_mode.suffix = this._private.file_mode.suffix.concat('.', this._private.scope_name)
        }
    }

    /**
     * stop (if worked) and start logger
     * @param {function} [callback]
     */
    start(callback) {
        this._private.sql_mode.local_timezone = (new Date()).getTimezoneOffset()
        this.pipe_on()
        app_timer.clear_list(this)
        app_timer.save_to_sql_start(this, () => {
            app_timer.save_to_file_start(this, () => {
                app_timer.delete_old_sql_start(this, () => {
                    app_timer.delete_old_file_start(this, () => {
                        if (lib_conv.isFunction(callback)) {
                            callback()
                        }
                    })
                })
            })
        })
    }

    /**
     * stop logger
     * @param {function} [callback]
     */
    stop(callback) {
        app_timer.save_to_sql_stop(this, () => {
            app_timer.save_to_file_stop(this, () => {
                app_timer.delete_old_sql_stop(this, () => {
                    app_timer.delete_old_file_stop(this, () => {
                        if (lib_conv.isFunction(callback)) {
                            callback()
                        }
                    })
                })
            })
        })
    }

    /** @param {'on'|'on_if_no_mssql'|'off'} param */
    set_file_mode_mode(param) {
        this._private.file_mode.mode = param
    }

    /**
     * path to storage log files, example - if you indicate path c:\myapp\, path storage c:\myapp\log\
     * @param {string} param
     */
    set_file_mode_path(param) {
        let path = lib_conv.toString(param, '')
        if (lib_conv.isEmpty(path)) {
            this._private.file_mode.path = undefined
            this._private.file_mode.path_trace = undefined
        } else {
            path = lib_path.join(path, 'log')
            let path_trace = lib_path.join(path, 'trace')

            if (!lib_conv.isEmpty(this._private.app_name)) {
                path_trace = lib_path.join(path_trace, this._private.app_name)
            }
            if (!lib_conv.isEmpty(this._private.scope_name)) {
                path_trace = lib_path.join(path_trace, this._private.scope_name)
            }

            lib_fs.ensureDirSync(path)
            lib_fs.ensureDirSync(path_trace)
            this._private.file_mode.path = path
            this._private.file_mode.path_trace = path_trace
        }
    }

    /**
     * schedule (in sec) by which data is saved to a file
     * @param {number} param
     */
    set_file_mode_time_save(param) {
        this._private.file_time_save_sec = lib_conv.toInt(param, 1)
        if (this._private.file_time_save_sec <= 0) {
            this._private.file_time_save_sec = 1
        }

    }

    /**
     * schedule (in sec) by which data is saved to a sql
     * @param {number} param
     */
    set_sql_mode_time_save(param) {
        this._private.sql_time_save_sec = lib_conv.toInt(param, 5)
        if (this._private.sql_time_save_sec <= 0) {
            this._private.sql_time_save_sec = 5
        }
    }

    /**
     * lifetime file log (in days), examples: 0 - today, 1 - today and yesterday
     * @param {number} param
     */
    set_file_mode_time_life(param) {
        this._private.file_time_life_day = lib_conv.toInt(param, 1)
        if (this._private.file_time_life_day < 0) {
            this._private.file_time_life_day = 1
        }
    }

    /**
     * lifetime sql log (in sec)
     * @param {number} param
     */
    set_sql_mode_time_life(param) {
        this._private.sql_time_life_sec = lib_conv.toInt(param, 86400)
        if (this._private.sql_time_life_sec <= 0) {
            this._private.sql_time_life_sec = 86400
        } else if (this._private.sql_time_life_sec > 2147483647) { //max int in MS SQL
            this._private.sql_time_life_sec = 2147483647
        }
    }

    /** @param {string} [pipe] */
    pipe_on(pipe) {
        let p = lib_conv.toString(pipe, '').trim().toLowerCase()
        if (lib_conv.isEmpty(p)) {
            this._private.allow_empty_pipe = true
            return
        }
        if (p === 'vmst-logger') {
            throw new Error ('this pipe name forbidden')
        }
        if (!this._private.pipe_list.some(f => f === p)) {
            this._private.pipe_list.push(p)
        }
    }

    /** @param {string} [pipe] */
    pipe_off(pipe) {
        let p = lib_conv.toString(pipe, '').trim().toLowerCase()
        if (lib_conv.isEmpty(p)) {
            this._private.allow_empty_pipe = false
            return
        }
        this._private.pipe_list = this._private.pipe_list.filter(f => f !== p)
    }

    /**
     * @callback callback_log
     * @param {type.log} message
     *//**
     * @param {string|Error} data
     * @param {string|string[]} [pipes]
     * @param {type.log_trace|type.log_trace[]} [traces]
     * @param {callback_log} [callback] service callback, not for use outside the library
     */
    log(data, pipes, traces, callback) {
        let callback_exists = lib_conv.isFunction(callback)
        if (app_message.allow_by_pipes(this, pipes, callback_exists) !== true) return
        let message = app_message.message_beautify(this, false, data, pipes, traces, callback_exists)
        if (lib_conv.isAbsent(message)) return

        if (this._private.print_to_console === true) {
            console.log(message.verbal_message)
        }

        this._private.log_list.push(message)
        if (callback_exists) {
            callback(message)
        }
    }

    /**
     * @callback callback_error
     * @param {type.log} message
     *//**
     * @param {string|Error} data
     * @param {string|string[]} [pipes]
     * @param {type.log_trace|type.log_trace[]} [traces]
     * @param {callback_error} [callback] service callback, not for use outside the library
     */
    error(data, pipes, traces, callback) {
        let callback_exists = lib_conv.isFunction(callback)
        if (app_message.allow_by_pipes(this, pipes, callback_exists) !== true) return
        let message = app_message.message_beautify(this, true, data, pipes, traces, callback_exists)
        if (lib_conv.isAbsent(message)) return

        if (this._private.print_to_console === true) {
            console.error(message.verbal_message)
        }

        this._private.log_list.push(message)
        if (callback_exists) {
            callback(message)
        }
    }
}

module.exports = App