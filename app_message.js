//@ts-check
const lib_path = require('path')
const lib_os = require('os')
const lib_uuid = require('uuid-random')
const lib_conv = require('viva-convert')
const type = require('./@type.js')
const app = require('./app.js')

exports.pipe_beautify = pipe_beautify
exports.traces_beautify = traces_beautify
exports.message_beautify = message_beautify
exports.allow_by_pipes = allow_by_pipes

/**
 * @param {string|string[]} pipes
 * @param {boolean} allow_pipe_vmst_logger
 * @returns {string[]}
 */
function pipe_beautify(pipes, allow_pipe_vmst_logger) {
    let pipe_list = []
    if (allow_pipe_vmst_logger === true) {
        pipe_list.push('vmst-logger')
    }
    if (lib_conv.isAbsent(pipes)) return pipe_list
    if (Array.isArray(pipes)) {
        return pipe_list.concat(pipes.map(m => { return lib_conv.toString(m,'').trim().toLowerCase() })).filter(f => !lib_conv.isEmpty(f))
    } else {
        let single_pipe = lib_conv.toString(pipes,'').trim().toLowerCase()
        if (lib_conv.isEmpty(single_pipe)) {
            return pipe_list
        }
        return pipe_list.concat([single_pipe])
    }
}

/**
 * @param {type.log_trace|type.log_trace[]} traces
 * @returns {type.log_trace[]}
 */
function traces_beautify(traces) {
    if (lib_conv.isAbsent(traces)) return []
    if (Array.isArray(traces)) {
        return [].concat(traces.map(m => { return {kind: lib_conv.toString(m.kind,'').trim().toLowerCase(), data: lib_conv.toString(m.data, '') }})).filter(f => !lib_conv.isEmpty(f.data))
    } else {
        let single_trace = {
            kind: lib_conv.toString(traces.kind,'').trim().toLowerCase(),
            data: lib_conv.toString(traces.data,'')
        }
        if (lib_conv.isEmpty(single_trace.data)) {
            return []
        } else {
            return [single_trace]
        }
    }
}

/**
 * @param {app} app
 * @param {boolean} is_error
 * @param {string|Error} data
 * @param {string|string[]} pipes
 * @param {type.log_trace|type.log_trace[]} traces
 * @param {boolean} allow_pipe_vmst_logger
 * @returns {type.log}
 */
function message_beautify(app, is_error, data, pipes, traces, allow_pipe_vmst_logger) {
    let data_string = (typeof data === 'object' ? lib_conv.toErrorMessage(data) : lib_conv.toString(data,''))
    if (lib_conv.isEmpty(data_string)) return undefined

    let fdm = new Date()
    let pipe_list = pipe_beautify(pipes, allow_pipe_vmst_logger)
    let trace_list = traces_beautify(traces)
    let file_name_trace = undefined

    let verbal_message = ''

    if (app._private.print_to_console === true || (!lib_conv.isEmpty(app._private.file_mode.path) && !lib_conv.isEmpty(app._private.file_mode.path_trace))) {
        verbal_message = lib_conv.formatDate(fdm, 126)
        verbal_message = verbal_message.concat((is_error === true ? ' ERROR' : ' DEBUG'))
        if (pipe_list.length > 0) {
            verbal_message = verbal_message.concat(' #PIPES#={', pipe_list.join('}{') ,'}')
        }
        verbal_message = verbal_message.concat(' ', lib_conv.toString(data_string, ''))

        if (trace_list.length > 0 && !lib_conv.isEmpty(app._private.file_mode.path) && !lib_conv.isEmpty(app._private.file_mode.path_trace)) {
            let trace_uuid = lib_uuid()
            file_name_trace = lib_path.join(app._private.file_mode.path_trace, lib_conv.formatDate(fdm, 112), trace_uuid.concat('.log'))

            verbal_message = verbal_message.concat(lib_os.EOL, '#TRACE#='.concat(file_name_trace))
        }
    }

    return {
        data: data_string,
        verbal_message: verbal_message,
        pipe_list: pipe_list,
        trace_list: trace_list,
        is_error: is_error,
        fdm: fdm,
        write_to_file: false,
        write_to_mssql: false,
        file_name_trace: file_name_trace,
        last_write_log_has_error: false
    }
}

/**
 * @param {app} app
 * @param {string|string[]} pipes
 * @param {boolean} allow_pipe_vmst_logger
 * @returns {boolean}
 */
function allow_by_pipes(app, pipes, allow_pipe_vmst_logger) {
    if (lib_conv.isAbsent(pipes) && app._private.allow_empty_pipe === true) {
        return true
    }
    if (allow_pipe_vmst_logger === true) {
        return true
    }
    let pipe_list_beautify = pipe_beautify(pipes, allow_pipe_vmst_logger)
    for (let i = 0; i < pipe_list_beautify.length; i++) {
        if (app._private.pipe_list.includes(pipe_list_beautify[i])) {
            return true
        }
    }
    return false
}